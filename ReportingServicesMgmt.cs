﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using ReportSync.Properties;
using ReportSync.ReportService;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ReportSync
{
    public class ReportingServicesMgmt
    {
        private const string ServicesUrl = @"/ReportService2005.asmx";
        public ReportingService2005 ReportingService { get; private set; }
        public Dictionary<string, string> DataSources { get; set; }
        public ReportingServicesMgmt(string url, string username, string password, bool integratedAuth)
        {
            var reportServerUrl = url.TrimEnd('/') + ServicesUrl;
            string domain = "";
            try
            {
                domain = reportServerUrl.Replace("http://", "").Replace("/ReportServer", "").Split(':')[0];
            }
            catch (Exception) { }

            DataSources = new Dictionary<string, string>();

            ReportingService = new ReportingService2005 {Url = reportServerUrl};
            if (!integratedAuth)
            {
                var nameParts = username.Split('\\', '/');
                if (nameParts.Length > 2)
                {
                    throw new Exception(Resources.Incorrect_destination_user_name);
                }
                ReportingService.Credentials = nameParts.Length == 2
                    ? new System.Net.NetworkCredential(nameParts[1], password, nameParts[0])
                    : new System.Net.NetworkCredential(username, password);
            }
        }

        public async Task<bool> FormAuthAsync(string url, string username, string password)
        {
            string domain = "";
            try
            {
                domain = url.Replace("http://", "").Replace("/ReportServer", "").Split(':')[0];
            }
            catch (Exception) { }

            CookieContainer cookies = new CookieContainer();
            HttpClientHandler handler = new HttpClientHandler();
            handler.CookieContainer = cookies;

            HttpClient client = new HttpClient(handler);
            //var byteArray = Encoding.ASCII.GetBytes($"{username}:{password}");
            //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            //HttpContent content = new StringContent("");
            //HttpResponseMessage response = await client.PostAsync($"{url}/logon.aspx", content);
            string uriDomain = url.Replace("/ReportServer", "");

            var postObj = new
            {
                UserName = username,
                Password = password
            };
            var json = JsonConvert.SerializeObject(postObj);
            HttpContent postContent = new StringContent(json, Encoding.UTF8, "application/json");
            try
            {
                var result = await client.PostAsync($"{uriDomain}/reports/api/v2.0/Session", postContent);
                result.EnsureSuccessStatusCode();

                Uri uri = new Uri(uriDomain);
                ReportingService.CookieContainer = new CookieContainer();
                IEnumerable<Cookie> responseCookies = cookies.GetCookies(uri).Cast<Cookie>();
                foreach (Cookie cookie in responseCookies)
                {
                    ReportingService.CookieContainer.Add(new Cookie(cookie.Name, cookie.Value, "/", domain));
                }

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
